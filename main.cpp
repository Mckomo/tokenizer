//
//  main.cpp
//  Tokenizer
//
//  Created by Maciej Komorowski on 09.05.2012.
//  Copyright (c) 2012 Mckomo. All rights reserved.
//

#include <iostream>


#include "SourceStream.h"
#include "Tokenizer.h"
#include "Token.h"

#include "Character.h"

using namespace std;

int main() {
    
    TextSourceStream s("<post id=\"test\">Maciek</post>");
    Tokenizer t(&s);
    Token* token;
    
    
    for( int i=0; i<8; i++) {
        
        token = t.getNextToken(); 
        
        switch ( token->getType() ) {
                
            case START_TAG_TOKEN:
                cout << endl <<
                    "[" <<
                    ((TagToken*) token)->name << " " <<
                    ((TagToken*) token)->attribute.name <<
                    "=" <<
                    ((TagToken*) token)->attribute.value <<
                    "]" << endl;
                break;
                
            case END_TAG_TOKEN:
                cout << endl <<
                "[/" <<
                ((TagToken*) token)->name <<
                "]" << endl;
                break;
                
            case CHARAKTER_TOKEN:
                cout << ((CharakterToken*) token)->value;
                break;
        }
    }


    return 0;
};


