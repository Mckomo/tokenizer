//
//  SourceStream.cpp
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "SourceStream.h"

using namespace std;

// KeyboardSourceStream

char KeyboardSourceStream::getNextChar() {
    char c;
    cin >> c;
    return c;
};


TextSourceStream::TextSourceStream() {
    currentChar = 0; 
    content = "<post id=\"test\">Maciek</post>"; //len 22
};

TextSourceStream::TextSourceStream(char *stream) {
    currentChar = 0; 
    content = stream;
};

// TextSourceStream


char TextSourceStream::getNextChar() {
    char c;
    c = content[currentChar++];
    return c;
};

// CurlSourceStream
    
char CurlSourceStream::getNextChar() {
    return NULL;
};
