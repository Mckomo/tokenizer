#include "Character.h"
#include <string>

// Charakter

char Character::getValue() {
    return value;
}

bool Character::isNull() {
    
    if( value ==  0x0000 ) 
        return true;
    else
        return false;
};

bool Character::isCharacterTabulation() {
    
    if( value ==  CHARACTER_TABULATION ) 
        return true;
    else
        return false;
};

bool Character::isLineFeed() {
    
    if( value ==  LINE_FEED ) 
        return true;
    else
        return false;
};

bool Character::isFormFeed() {
    
    if( value ==  FORM_FEED ) 
        return true;
    else
        return false;
};

bool Character::isSpace() {
    
    if( value ==  SPACE ) 
        return true;
    else
        return false;
};

bool Character::isExclamationMark() {
    
    if( value ==  EXCLAMATION_MARK ) 
        return true;
    else
        return false;
};

bool Character::isQuotationMark() {
    
    if( value ==  QUOTATION_MARK ) 
        return true;
    else
        return false;
};

bool Character::isNumberSign() {
    
    if( value ==  NUMBER_SIGN ) 
        return true;
    else
        return false;
};

bool Character::isAmpersand() {
    
    if( value ==  AMPERSAND ) 
        return true;
    else
        return false;
};

bool Character::isApostrophe() {
    
    if( value ==  APOSTROPHE ) 
        return true;
    else
        return false;
};

bool Character::isHyphenMinus() {
    
    if( value ==  HYPHEN_MINUS ) 
        return true;
    else
        return false;
};

bool Character::isSolidus() {
    
    if( value ==  SOLIDUS ) 
        return true;
    else
        return false;
};

bool Character::isLessThanSign() {
    
    if( value ==  LESS_THAN_SIGN ) 
        return true;
    else
        return false;
};

bool Character::isEqualsSign() {
    
    if( value ==  EQUALS_SIGN ) 
        return true;
    else
        return false;
};

bool Character::isGreaterThanSign() {
    
    if( value ==  GREATER_THAN_SIGN ) 
        return true;
    else
        return false;
};

bool Character::isQuestionMark() {
    
    if( value ==  QUESTION_MARK ) 
        return true;
    else
        return false;
};

bool Character::isGraveAccent() {
    
    if( value ==  GRAVE_ACCENT ) 
        return true;
    else
        return false;
};

bool Character::isEOF() {
    
    if( value ==  EOF ) 
        return true;
    else
        return false;
};


bool Character::isCapitalLetter() {
    // A-Z
    if( value >= 0x0041 && value <= 0x005A  )
        return true;
    else
        return false;
};

bool Character::isSmallLetter() {
    // a-z
    if( value >= 0x0061 && value <= 0x007A  )
        return true;
    else
        return false;
};

bool Character::isWhiteSign(){
    
    if( isCharacterTabulation() || isLineFeed() || isFormFeed() || isSpace() )
        return true;
    else
        return false;
};


Character& Character::operator= (char c) {
    value = c;
    return *this;
};

Character& Character::operator= (char* c) {
    value = *c;
    return *this;
};

Character::operator char() {
    return value;
};

// AdditionalCharakter


AdditionalCharacter& AdditionalCharacter::operator= (char c) {
    value = c;
    return *this;
};

AdditionalCharacter& AdditionalCharacter::operator= (char* c) {
    value = *c;
    return *this;
};

AdditionalCharacter::operator char() {
    char tmp = value;
    value = NULL;
    return tmp;
};
