//
//  Tokenizer.cpp
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "Tokenizer.h"

using namespace std;

const string TokenizerStateName[69] = {
    "DATA_STATE",
	"CHARACTER_REFERENCE_IN_DATA_STATE",
	"RCDATA_STATE",
	"CHARACTER_REFERENCE_IN_RCDATA_STATE",
	"RAWTEXT_STATE",
	"SCRIPT_DATA_STATE",
	"PLAINTEXT_STATE",
	"TAG_OPEN_STATE",
	"END_TAG_OPEN_STATE",
	"TAG_NAME_STATE",
	"RCDATA_LESSTHAN_SIGN_STATE",
	"RCDATA_END_TAG_OPEN_STATE",
	"RCDATA_END_TAG_NAME_STATE",
	"RAWTEXT_LESSTHAN_SIGN_STATE",
	"RAWTEXT_END_TAG_OPEN_STATE",
	"RAWTEXT_END_TAG_NAME_STATE",
	"SCRIPT_DATA_LESSTHAN_SIGN_STATE",
	"SCRIPT_DATA_END_TAG_OPEN_STATE",
	"SCRIPT_DATA_END_TAG_NAME_STATE",
	"SCRIPT_DATA_ESCAPE_START_STATE",
	"SCRIPT_DATA_ESCAPE_START_DASH_STATE",
	"SCRIPT_DATA_ESCAPED_STATE",
	"SCRIPT_DATA_ESCAPED_DASH_STATE",
	"SCRIPT_DATA_ESCAPED_DASH_DASH_STATE",
	"SCRIPT_DATA_ESCAPED_LESSTHAN_SIGN_STATE",
	"SCRIPT_DATA_ESCAPED_END_TAG_OPEN_STATE",
	"SCRIPT_DATA_ESCAPED_END_TAG_NAME_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPE_START_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPED_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPED_DASH_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPED_LESSTHAN_SIGN_STATE",
	"SCRIPT_DATA_DOUBLE_ESCAPE_END_STATE",
	"BEFORE_ATTRIBUTE_NAME_STATE",
	"ATTRIBUTE_NAME_STATE",
	"AFTER_ATTRIBUTE_NAME_STATE",
	"BEFORE_ATTRIBUTE_VALUE_STATE",
	"ATTRIBUTE_VALUE_DOUBLEQUOTED_STATE",
	"ATTRIBUTE_VALUE_SINGLEQUOTED_STATE",
	"ATTRIBUTE_VALUE_UNQUOTED_STATE",
	"CHARACTER_REFERENCE_IN_ATTRIBUTE_VALUE_STATE",
	"AFTER_ATTRIBUTE_VALUE_QUOTED_STATE",
	"SELFCLOSING_START_TAG_STATE",
	"BOGUS_COMMENT_STATE",
	"MARKUP_DECLARATION_OPEN_STATE",
	"COMMENT_START_STATE",
	"COMMENT_START_DASH_STATE",
	"COMMENT_STATE",
	"COMMENT_END_DASH_STATE",
	"COMMENT_END_STATE",
	"COMMENT_END_BANG_STATE",
	"DOCTYPE_STATE",
	"BEFORE_DOCTYPE_NAME_STATE",
	"DOCTYPE_NAME_STATE",
	"AFTER_DOCTYPE_NAME_STATE",
	"AFTER_DOCTYPE_PUBLIC_KEYWORD_STATE",
	"BEFORE_DOCTYPE_PUBLIC_IDENTIFIER_STATE",
	"DOCTYPE_PUBLIC_IDENTIFIER_DOUBLEQUOTED_STATE",
	"DOCTYPE_PUBLIC_IDENTIFIER_SINGLEQUOTED_STATE",
	"AFTER_DOCTYPE_PUBLIC_IDENTIFIER_STATE",
	"BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS_STATE",
	"AFTER_DOCTYPE_SYSTEM_KEYWORD_STATE",
	"BEFORE_DOCTYPE_SYSTEM_IDENTIFIER_STATE",
	"DOCTYPE_SYSTEM_IDENTIFIER_DOUBLEQUOTED_STATE",
	"DOCTYPE_SYSTEM_IDENTIFIER_SINGLEQUOTED_STATE",
	"AFTER_DOCTYPE_SYSTEM_IDENTIFIER_STATE",
	"BOGUS_DOCTYPE_STATE",
	"CDATA_SECTION_STATE",
	"TOKENIZING_CHARACTER_REFERENCES_STATE"
};

int Tokenizer::errorCount = 0;

// Tokenizer workflow functions

Token* Tokenizer::getNextToken() {
    tokenReady = false;
    do {
        processNextCharacter(); 
    } while (!tokenReady);
    return currentToken;
};

void Tokenizer::emitToken() {
    tokenReady = true;
    
    if ( mode == "DEBUG") 
        cout << "EMITIED TOKEN" << endl;
    
};

void Tokenizer::emitToken(Token* token) {
    //TokenStream.append(token);
};

void Tokenizer::switchState(TokenizerState state) {
    currentState = state;
};

void Tokenizer::parseErorr(int p) {
    
    errorCount++;
    
    try {
        throw p;
    } catch (int p) {
        
        cout << endl <<
                "Issue number: " << errorCount << endl
             << "Parse error occured. Exeption number " << p << "." << endl
             << "Current tokenizer state: " << TokenizerStateName[currentState] << "." << endl
             << "Current characetr: " << character << "." << endl
             << "Temperary buffor dump: " << temporaryBuffer << "<- ERORR HERE" << endl << endl;
    }
};

// Usefull tools 

string Tokenizer::getCurrentStateName() {
    return TokenizerStateName[currentState];
};
    
// Functions matching current state 
    
void Tokenizer::dataState() {
        
    if( character.isAmpersand() ) 
        switchState(CHARACTER_REFERENCE_IN_DATA_STATE);
    
    else if ( character.isLessThanSign() )
        switchState(TAG_OPEN_STATE);
    
    else if ( character.isNull() ) {
        parseErorr();
        currentToken = new CharakterToken(character);
    }
    
    else if ( character == EOF ) {
        currentToken = new EndOfLineToken;
        emitToken();
    }
    
    else {
        currentToken = new CharakterToken(character);
        emitToken();
    }
    
};
    
void Tokenizer::tagOpenState() {

    if ( character.isExclamationMark() ) 
        switchState(MARKUP_DECLARATION_OPEN_STATE);
    
    else if ( character.isSolidus() )
        switchState(END_TAG_OPEN_STATE);

    else if ( character.isCapitalLetter() ) {
        currentToken = new StartTagToken;
        ((TagToken*) currentToken)->appendName(character+0x0020);
        switchState(TAG_NAME_STATE);
    }

    else if ( character.isSmallLetter() ) {
        currentToken = new StartTagToken;
        ((TagToken*) currentToken)->appendName(character);
        switchState(TAG_NAME_STATE);
    }

    else if ( character.isQuestionMark() ) {
        parseErorr();
        switchState(BOGUS_COMMENT_STATE);
    }
    else {
        parseErorr();
        currentToken = new CharakterToken(0x3c);
        dataState();
        
    }
    
};
    
    
void Tokenizer::tagNameState() {
        
    if ( character.isWhiteSign() )
        switchState(BEFORE_ATTRIBUTE_NAME_STATE);
    
    else if ( character.isSolidus() )
        switchState(SELF_CLOSING_START_TAG_STATE);

    else if ( character.isGreaterThanSign() ) {
        switchState(DATA_STATE);
        emitToken();
    }

    else if ( character.isCapitalLetter() ) 
        ((TagToken*) currentToken)->appendName(character+0x20); 
    
    else if ( character.isNull() ) {
        parseErorr();
        ((TagToken*) currentToken)->appendName(0xfffd);
    }

    else if ( character.isEOF() ) {
        parseErorr();
        dataState();
    }
    
    else 
        ((TagToken*) currentToken)->appendName(character);
};
  

void Tokenizer::endTagOpenState() {
    if ( character.isCapitalLetter() ) {
        currentToken = new EndTagToken();
        ((TagToken*) currentToken)->appendName(character+0x20);
        switchState(TAG_NAME_STATE);
    }
    
    else if ( character.isSmallLetter() ) {
        currentToken = new EndTagToken();
        ((TagToken*) currentToken)->appendName(character);
        switchState(TAG_NAME_STATE);
    }
    
    else if ( character.isGreaterThanSign() ) {
        parseErorr();
        emitToken();
    }
    
    else if ( character.isEOF() ) {
        parseErorr();
        emitToken(new CharakterToken(0x003c));
        emitToken(new CharakterToken(0x002f));
        dataState();
    }
    
    else {
        parseErorr();
        switchState(BOGUS_COMMENT_STATE);
    }
};
    
void Tokenizer::beforeAttributeNameState() {
            
    if ( character.isWhiteSign() )
        return;

    else if ( character.isSolidus() )
        switchState(SELF_CLOSING_START_TAG_STATE);

    else if ( character.isGreaterThanSign() ) {
        switchState(DATA_STATE);
        this->emitToken();
    }
    
    else if ( character.isCapitalLetter() ) {
        ((TagToken*) currentToken)->appendAttributeName(character+0x20);
        switchState(ATTRIBUTE_NAME_STATE);
    }
    
    else {
        ((TagToken*) currentToken)->appendAttributeName(character);
        switchState(ATTRIBUTE_NAME_STATE);  
    }
    
};

void Tokenizer::attributeNameState() {
        
    if ( character.isWhiteSign() )
        switchState(AFTER_ATTRIBUTE_NAME_STATE);
    
    else if ( character.isSolidus() )
        switchState(SELF_CLOSING_START_TAG_STATE);
    
    else if ( character.isEqualsSign() ) 
        switchState(BEFORE_ATTRIBUTE_VALUE_STATE);
    
    else if ( character.isGreaterThanSign() ) {
        switchState(DATA_STATE);
        this->emitToken();
    }
    
    else if ( character.isCapitalLetter() ) {
        ((TagToken*) currentToken)->appendAttributeName(character+0x0020);
        switchState(ATTRIBUTE_NAME_STATE);
    }
    
    else if ( character.isNull() ) {
        parseErorr();
        ((TagToken*) currentToken)->appendAttributeName(0xFFFD);
    }
    
    else if ( character.isQuestionMark() || character.isApostrophe() || character.isLessThanSign() ) {
        parseErorr();
        ((TagToken*) currentToken)->appendAttributeName(character); // like in else
    }
            
    else if ( character.isEOF() ) {
        switchState(DATA_STATE);
        dataState();
    }
    else {
        ((TagToken*) currentToken)->appendAttributeName(character);
    }
    
};

void Tokenizer::beforeAttributeValueState() {
        
    if ( character.isWhiteSign() )
        return;
    
    else if ( character.isQuotationMark() ) 
        switchState(ATTRIBUTE_VALUE_DOUBLEQUOTED_STATE);
    
    else if ( character.isAmpersand() ) 
        switchState(ATTRIBUTE_VALUE_UNQUOTED_STATE);
    
    else if ( character.isApostrophe() ) 
        switchState(ATTRIBUTE_VALUE_SINGLEQUOTED_STATE);
    
    else if ( character.isNull() ) {
        parseErorr();
        ((TagToken*) currentToken)->appendAttributeValue(0xFFFD);
    }
    
    else if ( character.isGreaterThanSign() ) {
        parseErorr();
        switchState(DATA_STATE);
        emitToken();
    }
    
    else if ( character.isLessThanSign() || character.isEqualsSign() ||  character.isGraveAccent() ) {
        parseErorr();
        // Like in anything else 
        ((TagToken*) currentToken)->appendAttributeValue(character); // przerobic wsrtring
        switchState(ATTRIBUTE_VALUE_UNQUOTED_STATE);  
    }
    
    else if( character.isEOF() ){
        parseErorr();
        dataState();
    }
    
    else {
        ((TagToken*) currentToken)->appendAttributeValue(character); // przerobic wsrtring
        switchState(ATTRIBUTE_VALUE_UNQUOTED_STATE);  
    }
    
};

void Tokenizer::attributeValueDoublequotedState() {
    
    if ( character.isQuotationMark() ) 
        switchState(AFTER_ATTRIBUTE_VALUE_QUOTED_STATE);
    
    else if ( character.isAmpersand() ) {
        additionalCharacter = 0x0022;
        switchState(CHARACTER_REFERENCE_IN_ATTRIBUTE_VALUE_STATE);
        //CHARACTER_REFERENCE
    }
    
    else if ( character.isNull() ) {
        parseErorr();
        ((TagToken*) currentToken)->appendAttributeValue(0xFFFD);
    }
    
    else if( character.isEOF() ){
        parseErorr();
        dataState();
    }
    
    else {
        ((TagToken*) currentToken)->appendAttributeValue(character); // przerobic wsrtring
    }
    
};

void Tokenizer::afterAttributeValueQuotedState() {
    
    if ( character.isWhiteSign() )
        switchState(BEFORE_ATTRIBUTE_NAME_STATE);
    
    else if ( character.isSolidus() )
        switchState(SELF_CLOSING_START_TAG_STATE);
    
    else if ( character.isGreaterThanSign() ) {
        switchState(DATA_STATE);
        emitToken();
    }

    
    else if( character.isEOF() ){
        parseErorr();
        dataState();
    }
    
    else {
        parseErorr();
        beforeAttributeNameState();
    }
    
};

void Tokenizer::selfClosingStartTagState() {
    
    if ( character.isGreaterThanSign() ) {
        ((StartTagToken*) currentToken)->selfClosing = true;
        switchState(DATA_STATE);
        emitToken();
    }
    
    
    else if( character.isEOF() ){
        parseErorr();
        dataState();
    }
    
    else {
        parseErorr();
        beforeAttributeNameState();
    }
    
};

// Constructors

Tokenizer::Tokenizer(SourceStream* s) {
    sourceStream = s;
    currentState = DATA_STATE;
    currentToken = NULL;
    tokenReady = false;
};

Tokenizer::Tokenizer(SourceStream* s, string tokenizerMode) {
    
    sourceStream = s;
    currentState = DATA_STATE; // Czy da się to jakoś zastąpić ?
    currentToken = NULL;
    tokenReady = false;
    
    mode = tokenizerMode;
    
};

// Rest of state functions


void Tokenizer::characterReferenceInDataState() {
	switchState(DATA_STATE);
};

void Tokenizer::rcdataState() {
	switchState(DATA_STATE);
};

void Tokenizer::characterReferenceInRcdataState() {
	switchState(DATA_STATE);
};

void Tokenizer::rawtextState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataState() {
	switchState(DATA_STATE);
};

void Tokenizer::plaintextState() {
	switchState(DATA_STATE);
};

void Tokenizer::rcdataLessthanSignState() {
	switchState(DATA_STATE);
};

void Tokenizer::rcdataEndTagOpenState() {
	switchState(DATA_STATE);
};

void Tokenizer::rcdataEndTagNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::rawtextLessthanSignState() {
	switchState(DATA_STATE);
};

void Tokenizer::rawtextEndTagOpenState() {
	switchState(DATA_STATE);
};

void Tokenizer::rawtextEndTagNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataLessthanSignState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEndTagOpenState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEndTagNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapeStartState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapeStartDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedDashDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedLessthanSignState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedEndTagOpenState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataEscapedEndTagNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapeStartState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapedState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapedDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapedDashDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapedLessthanSignState() {
	switchState(DATA_STATE);
};

void Tokenizer::scriptDataDoubleEscapeEndState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterAttributeNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::attributeValueSinglequotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::attributeValueUnquotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::characterReferenceInAttributeValueState() {
	switchState(DATA_STATE);
};

void Tokenizer::bogusCommentState() {
	switchState(DATA_STATE);
};

void Tokenizer::markupDeclarationOpenState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentStartState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentStartDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentEndDashState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentEndState() {
	switchState(DATA_STATE);
};

void Tokenizer::commentEndBangState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypeState() {
	switchState(DATA_STATE);
};

void Tokenizer::beforeDoctypeNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypeNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterDoctypeNameState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterDoctypePublicKeywordState() {
	switchState(DATA_STATE);
};

void Tokenizer::beforeDoctypePublicIdentifierState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypePublicIdentifierDoublequotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypePublicIdentifierSinglequotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterDoctypePublicIdentifierState() {
	switchState(DATA_STATE);
};

void Tokenizer::betweenDoctypePublicAndSystemIdentifiersState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterDoctypeSystemKeywordState() {
	switchState(DATA_STATE);
};

void Tokenizer::beforeDoctypeSystemIdentifierState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypeSystemIdentifierDoublequotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::doctypeSystemIdentifierSinglequotedState() {
	switchState(DATA_STATE);
};

void Tokenizer::afterDoctypeSystemIdentifierState() {
	switchState(DATA_STATE);
};

void Tokenizer::bogusDoctypeState() {
	switchState(DATA_STATE);
};

void Tokenizer::cdataSectionState() {
	switchState(DATA_STATE);
};

void Tokenizer::tokenizingCharacterReferencesState() {
	switchState(DATA_STATE);
};

void Tokenizer::processNextCharacter() {
    
    character = sourceStream->getNextChar();
    temporaryBuffer += character;
    
    if ( mode == "DEBUG" ) {
        cout << endl
             << "TOKENIZATOR STATE: " << getCurrentStateName() << endl 
             << "INPUT SIGN: " << character << endl;
    }
    
    switch (currentState) {
            
        case DATA_STATE: 
            dataState(); 
            break; 
            
        case CHARACTER_REFERENCE_IN_DATA_STATE: 
            characterReferenceInDataState(); 
            break; 
            
        case RCDATA_STATE: 
            rcdataState(); 
            break; 
            
        case CHARACTER_REFERENCE_IN_RCDATA_STATE: 
            characterReferenceInRcdataState(); 
            break; 
            
        case RAWTEXT_STATE: 
            rawtextState(); 
            break; 
            
        case SCRIPT_DATA_STATE: 
            scriptDataState(); 
            break; 
            
        case PLAINTEXT_STATE: 
            plaintextState(); 
            break; 
            
        case TAG_OPEN_STATE: 
            tagOpenState(); 
            break; 
            
        case END_TAG_OPEN_STATE: 
            endTagOpenState(); 
            break; 
            
        case TAG_NAME_STATE: 
            tagNameState(); 
            break; 
            
        case RCDATA_LESSTHAN_SIGN_STATE: 
            rcdataLessthanSignState(); 
            break; 
            
        case RCDATA_END_TAG_OPEN_STATE: 
            rcdataEndTagOpenState(); 
            break; 
            
        case RCDATA_END_TAG_NAME_STATE: 
            rcdataEndTagNameState(); 
            break; 
            
        case RAWTEXT_LESSTHAN_SIGN_STATE: 
            rawtextLessthanSignState(); 
            break; 
            
        case RAWTEXT_END_TAG_OPEN_STATE: 
            rawtextEndTagOpenState(); 
            break; 
            
        case RAWTEXT_END_TAG_NAME_STATE: 
            rawtextEndTagNameState(); 
            break; 
            
        case SCRIPT_DATA_LESSTHAN_SIGN_STATE: 
            scriptDataLessthanSignState(); 
            break; 
            
        case SCRIPT_DATA_END_TAG_OPEN_STATE: 
            scriptDataEndTagOpenState(); 
            break; 
            
        case SCRIPT_DATA_END_TAG_NAME_STATE: 
            scriptDataEndTagNameState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPE_START_STATE: 
            scriptDataEscapeStartState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPE_START_DASH_STATE: 
            scriptDataEscapeStartDashState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_STATE: 
            scriptDataEscapedState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_DASH_STATE: 
            scriptDataEscapedDashState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_DASH_DASH_STATE: 
            scriptDataEscapedDashDashState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_LESSTHAN_SIGN_STATE: 
            scriptDataEscapedLessthanSignState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_END_TAG_OPEN_STATE: 
            scriptDataEscapedEndTagOpenState(); 
            break; 
            
        case SCRIPT_DATA_ESCAPED_END_TAG_NAME_STATE: 
            scriptDataEscapedEndTagNameState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPE_START_STATE: 
            scriptDataDoubleEscapeStartState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPED_STATE: 
            scriptDataDoubleEscapedState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPED_DASH_STATE: 
            scriptDataDoubleEscapedDashState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH_STATE: 
            scriptDataDoubleEscapedDashDashState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPED_LESSTHAN_SIGN_STATE: 
            scriptDataDoubleEscapedLessthanSignState(); 
            break; 
            
        case SCRIPT_DATA_DOUBLE_ESCAPE_END_STATE: 
            scriptDataDoubleEscapeEndState(); 
            break; 
            
        case BEFORE_ATTRIBUTE_NAME_STATE: 
            beforeAttributeNameState(); 
            break; 
            
        case ATTRIBUTE_NAME_STATE: 
            attributeNameState(); 
            break; 
            
        case AFTER_ATTRIBUTE_NAME_STATE: 
            afterAttributeNameState(); 
            break; 
            
        case BEFORE_ATTRIBUTE_VALUE_STATE: 
            beforeAttributeValueState(); 
            break; 
            
        case ATTRIBUTE_VALUE_DOUBLEQUOTED_STATE: 
            attributeValueDoublequotedState(); 
            break; 
            
        case ATTRIBUTE_VALUE_SINGLEQUOTED_STATE: 
            attributeValueSinglequotedState(); 
            break; 
            
        case ATTRIBUTE_VALUE_UNQUOTED_STATE: 
            attributeValueUnquotedState(); 
            break; 
            
        case CHARACTER_REFERENCE_IN_ATTRIBUTE_VALUE_STATE: 
            characterReferenceInAttributeValueState(); 
            break; 
            
        case AFTER_ATTRIBUTE_VALUE_QUOTED_STATE: 
            afterAttributeValueQuotedState(); 
            break; 
            
        case SELF_CLOSING_START_TAG_STATE: 
            selfClosingStartTagState(); 
            break; 
            
        case BOGUS_COMMENT_STATE: 
            bogusCommentState(); 
            break; 
            
        case MARKUP_DECLARATION_OPEN_STATE: 
            markupDeclarationOpenState(); 
            break; 
            
        case COMMENT_START_STATE: 
            commentStartState(); 
            break; 
            
        case COMMENT_START_DASH_STATE: 
            commentStartDashState(); 
            break; 
            
        case COMMENT_STATE: 
            commentState(); 
            break; 
            
        case COMMENT_END_DASH_STATE: 
            commentEndDashState(); 
            break; 
            
        case COMMENT_END_STATE: 
            commentEndState(); 
            break; 
            
        case COMMENT_END_BANG_STATE: 
            commentEndBangState(); 
            break; 
            
        case DOCTYPE_STATE: 
            doctypeState(); 
            break; 
            
        case BEFORE_DOCTYPE_NAME_STATE: 
            beforeDoctypeNameState(); 
            break; 
            
        case DOCTYPE_NAME_STATE: 
            doctypeNameState(); 
            break; 
            
        case AFTER_DOCTYPE_NAME_STATE: 
            afterDoctypeNameState(); 
            break; 
            
        case AFTER_DOCTYPE_PUBLIC_KEYWORD_STATE: 
            afterDoctypePublicKeywordState(); 
            break; 
            
        case BEFORE_DOCTYPE_PUBLIC_IDENTIFIER_STATE: 
            beforeDoctypePublicIdentifierState(); 
            break; 
            
        case DOCTYPE_PUBLIC_IDENTIFIER_DOUBLEQUOTED_STATE: 
            doctypePublicIdentifierDoublequotedState(); 
            break; 
            
        case DOCTYPE_PUBLIC_IDENTIFIER_SINGLEQUOTED_STATE: 
            doctypePublicIdentifierSinglequotedState(); 
            break; 
            
        case AFTER_DOCTYPE_PUBLIC_IDENTIFIER_STATE: 
            afterDoctypePublicIdentifierState(); 
            break; 
            
        case BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS_STATE: 
            betweenDoctypePublicAndSystemIdentifiersState(); 
            break; 
            
        case AFTER_DOCTYPE_SYSTEM_KEYWORD_STATE: 
            afterDoctypeSystemKeywordState(); 
            break; 
            
        case BEFORE_DOCTYPE_SYSTEM_IDENTIFIER_STATE: 
            beforeDoctypeSystemIdentifierState(); 
            break; 
            
        case DOCTYPE_SYSTEM_IDENTIFIER_DOUBLEQUOTED_STATE: 
            doctypeSystemIdentifierDoublequotedState(); 
            break; 
            
        case DOCTYPE_SYSTEM_IDENTIFIER_SINGLEQUOTED_STATE: 
            doctypeSystemIdentifierSinglequotedState(); 
            break; 
            
        case AFTER_DOCTYPE_SYSTEM_IDENTIFIER_STATE: 
            afterDoctypeSystemIdentifierState(); 
            break; 
            
        case BOGUS_DOCTYPE_STATE: 
            bogusDoctypeState(); 
            break; 
            
        case CDATA_SECTION_STATE: 
            cdataSectionState(); 
            break; 
            
        case TOKENIZING_CHARACTER_REFERENCES_STATE: 
            tokenizingCharacterReferencesState(); 
            break;
            
    }
};