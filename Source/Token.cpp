//
//  Token.cpp
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <string>
#include "Token.h"

using namespace std;

const string TokenTypeName[] = {
    "DOCTYPE_TOKEN",
    "START_TAG_TOKEN",
    "END_TAG_TOKEN",
    "CHARAKTER_TOKEN",
    "END_OF_LINE_TOKEN",
    "COMMENT_TOKEN",  
};

//Token

TokenType Token::getType() {
    return type;
};

string Token::getTypeName() {
    return TokenTypeName[type];
};

void Token::setType(TokenType tokenType){
    type = tokenType;
};

// DoctypeToken

DoctypeToken::DoctypeToken() {
    setType(DOCTYPE_TOKEN);
};

//TagToken

void TagToken::appendName(char c) {
    name += c;
};

void TagToken::startNewAttribute(char c) {
    appendAttributeName(c);
};

void TagToken::appendAttributeName(char c) {
    attribute.name += c; 
};

void TagToken::appendAttributeValue(char c) {
    attribute.value += c; 
};

//StartTagToken

StartTagToken::StartTagToken() {
    setType(START_TAG_TOKEN);
};

//EndTagToken

EndTagToken::EndTagToken() {
    setType(END_TAG_TOKEN);
};

// CharToken

CharakterToken::CharakterToken() {
    setType(CHARAKTER_TOKEN);
};

CharakterToken::CharakterToken(char c) {
    setType(CHARAKTER_TOKEN);
    setValue(c);
};

void CharakterToken::setValue(char c) {
    value = c;
};

//EndOfLineToken

EndOfLineToken::EndOfLineToken() {
    setType(END_OF_LINE_TOKEN);
    setValue(EOF);  
};

//Comment

CommentToken::CommentToken() {
    setType(COMMENT_TOKEN);
};

CommentToken::CommentToken(char c) {
    CommentToken::CommentToken();
    setValue(c);
};

