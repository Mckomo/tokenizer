//
//  Tokenizer.h
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Mac__Tokenizer_Tokenizer_h
#define Mac__Tokenizer_Tokenizer_h

#include <string>

#include "SourceStream.h"
#include "TokenStream.h"
#include "Token.h"
#include "Character.h"

using namespace std;


extern const string TokenizerStateName[];

enum TokenizerState {
    DATA_STATE,
    CHARACTER_REFERENCE_IN_DATA_STATE,
    RCDATA_STATE,
    CHARACTER_REFERENCE_IN_RCDATA_STATE,
    RAWTEXT_STATE,
    SCRIPT_DATA_STATE,
    PLAINTEXT_STATE,
    TAG_OPEN_STATE,
    END_TAG_OPEN_STATE,
    TAG_NAME_STATE,
    RCDATA_LESSTHAN_SIGN_STATE,
    RCDATA_END_TAG_OPEN_STATE,
    RCDATA_END_TAG_NAME_STATE,
    RAWTEXT_LESSTHAN_SIGN_STATE,
    RAWTEXT_END_TAG_OPEN_STATE,
    RAWTEXT_END_TAG_NAME_STATE,
    SCRIPT_DATA_LESSTHAN_SIGN_STATE,
    SCRIPT_DATA_END_TAG_OPEN_STATE,
    SCRIPT_DATA_END_TAG_NAME_STATE,
    SCRIPT_DATA_ESCAPE_START_STATE,
    SCRIPT_DATA_ESCAPE_START_DASH_STATE,
    SCRIPT_DATA_ESCAPED_STATE,
    SCRIPT_DATA_ESCAPED_DASH_STATE,
    SCRIPT_DATA_ESCAPED_DASH_DASH_STATE,
    SCRIPT_DATA_ESCAPED_LESSTHAN_SIGN_STATE,
    SCRIPT_DATA_ESCAPED_END_TAG_OPEN_STATE,
    SCRIPT_DATA_ESCAPED_END_TAG_NAME_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPE_START_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPED_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPED_DASH_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPED_LESSTHAN_SIGN_STATE,
    SCRIPT_DATA_DOUBLE_ESCAPE_END_STATE,
    BEFORE_ATTRIBUTE_NAME_STATE,
    ATTRIBUTE_NAME_STATE,
    AFTER_ATTRIBUTE_NAME_STATE,
    BEFORE_ATTRIBUTE_VALUE_STATE,
    ATTRIBUTE_VALUE_DOUBLEQUOTED_STATE,
    ATTRIBUTE_VALUE_SINGLEQUOTED_STATE,
    ATTRIBUTE_VALUE_UNQUOTED_STATE,
    CHARACTER_REFERENCE_IN_ATTRIBUTE_VALUE_STATE,
    AFTER_ATTRIBUTE_VALUE_QUOTED_STATE,
    SELF_CLOSING_START_TAG_STATE,
    BOGUS_COMMENT_STATE,
    MARKUP_DECLARATION_OPEN_STATE,
    COMMENT_START_STATE,
    COMMENT_START_DASH_STATE,
    COMMENT_STATE,
    COMMENT_END_DASH_STATE,
    COMMENT_END_STATE,
    COMMENT_END_BANG_STATE,
    DOCTYPE_STATE,
    BEFORE_DOCTYPE_NAME_STATE,
    DOCTYPE_NAME_STATE,
    AFTER_DOCTYPE_NAME_STATE,
    AFTER_DOCTYPE_PUBLIC_KEYWORD_STATE,
    BEFORE_DOCTYPE_PUBLIC_IDENTIFIER_STATE,
    DOCTYPE_PUBLIC_IDENTIFIER_DOUBLEQUOTED_STATE,
    DOCTYPE_PUBLIC_IDENTIFIER_SINGLEQUOTED_STATE,
    AFTER_DOCTYPE_PUBLIC_IDENTIFIER_STATE,
    BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS_STATE,
    AFTER_DOCTYPE_SYSTEM_KEYWORD_STATE,
    BEFORE_DOCTYPE_SYSTEM_IDENTIFIER_STATE,
    DOCTYPE_SYSTEM_IDENTIFIER_DOUBLEQUOTED_STATE,
    DOCTYPE_SYSTEM_IDENTIFIER_SINGLEQUOTED_STATE,
    AFTER_DOCTYPE_SYSTEM_IDENTIFIER_STATE,
    BOGUS_DOCTYPE_STATE,
    CDATA_SECTION_STATE,
    TOKENIZING_CHARACTER_REFERENCES_STATE
};

class Tokenizer {
    
    string mode;
    
    SourceStream *sourceStream;
    TokenStream *tokenStream;
    TokenizerState currentState;
    Token *currentToken;
    bool tokenReady;

    string temporaryBuffer; // przerobic na wstring
    
    static int errorCount;
    
    Character character;
    AdditionalCharacter additionalCharacter;
    
public:
    
    // Tokenizer workflow funcitons
    
    Token* getNextToken();
    void processNextCharacter();
    void emitToken();
    void emitToken(Token* token); 
    void switchState(TokenizerState state);
    void parseErorr(int p=0);
    
    // Usefull tools 
    
    string getCurrentStateName();
    
    // State functions
    
    void dataState();
    void characterReferenceInDataState();
    void rcdataState();
    void characterReferenceInRcdataState();
    void rawtextState();
    void scriptDataState();
    void plaintextState();
    void tagOpenState();
    void endTagOpenState();
    void tagNameState();
    void rcdataLessthanSignState();
    void rcdataEndTagOpenState();
    void rcdataEndTagNameState();
    void rawtextLessthanSignState();
    void rawtextEndTagOpenState();
    void rawtextEndTagNameState();
    void scriptDataLessthanSignState();
    void scriptDataEndTagOpenState();
    void scriptDataEndTagNameState();
    void scriptDataEscapeStartState();
    void scriptDataEscapeStartDashState();
    void scriptDataEscapedState();
    void scriptDataEscapedDashState();
    void scriptDataEscapedDashDashState();
    void scriptDataEscapedLessthanSignState();
    void scriptDataEscapedEndTagOpenState();
    void scriptDataEscapedEndTagNameState();
    void scriptDataDoubleEscapeStartState();
    void scriptDataDoubleEscapedState();
    void scriptDataDoubleEscapedDashState();
    void scriptDataDoubleEscapedDashDashState();
    void scriptDataDoubleEscapedLessthanSignState();
    void scriptDataDoubleEscapeEndState();
    void beforeAttributeNameState();
    void attributeNameState();
    void afterAttributeNameState();
    void beforeAttributeValueState();
    void attributeValueDoublequotedState();
    void attributeValueSinglequotedState();
    void attributeValueUnquotedState();
    void characterReferenceInAttributeValueState();
    void afterAttributeValueQuotedState();
    void selfClosingStartTagState();
    void bogusCommentState();
    void markupDeclarationOpenState();
    void commentStartState();
    void commentStartDashState();
    void commentState();
    void commentEndDashState();
    void commentEndState();
    void commentEndBangState();
    void doctypeState();
    void beforeDoctypeNameState();
    void doctypeNameState();
    void afterDoctypeNameState();
    void afterDoctypePublicKeywordState();
    void beforeDoctypePublicIdentifierState();
    void doctypePublicIdentifierDoublequotedState();
    void doctypePublicIdentifierSinglequotedState();
    void afterDoctypePublicIdentifierState();
    void betweenDoctypePublicAndSystemIdentifiersState();
    void afterDoctypeSystemKeywordState();
    void beforeDoctypeSystemIdentifierState();
    void doctypeSystemIdentifierDoublequotedState();
    void doctypeSystemIdentifierSinglequotedState();
    void afterDoctypeSystemIdentifierState();
    void bogusDoctypeState();
    void cdataSectionState();
    void tokenizingCharacterReferencesState();
    
    // Constructors
    Tokenizer(SourceStream* s);
    Tokenizer(SourceStream* s, string tokenizerMode);
    
};

#endif
