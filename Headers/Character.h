//
//  Character.h
//  Tokenizer
//
//  Created by Mckomo on 30.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Tokenizer_Character_h
#define Tokenizer_Character_h

class Character {
    
    char value; // przerobic na wchar_t
    
public:
    
	const static char CHARACTER_TABULATION = 0x0009;
	const static char LINE_FEED = 0x000A;
	const static char FORM_FEED = 0x000C;
	const static char SPACE = 0x0020;
	const static char EXCLAMATION_MARK = 0x0021;
	const static char QUOTATION_MARK = 0x0022;
	const static char NUMBER_SIGN = 0x0023;
	const static char AMPERSAND = 0x0026;
	const static char APOSTROPHE = 0x0027;
	const static char HYPHEN_MINUS = 0x002D;
	const static char SOLIDUS = 0x002F;
	const static char LESS_THAN_SIGN = 0x003C;
	const static char EQUALS_SIGN = 0x003D;
	const static char GREATER_THAN_SIGN = 0x003E;
	const static char QUESTION_MARK = 0x003F;
	const static char GRAVE_ACCENT = 0x0060;
   
    char getValue();
    
	bool isNull();
	bool isCharacterTabulation();
	bool isLineFeed();
	bool isFormFeed();
	bool isSpace();
	bool isExclamationMark();
	bool isQuotationMark();
	bool isNumberSign();
	bool isAmpersand();
	bool isApostrophe();
	bool isHyphenMinus();
	bool isSolidus();
	bool isLessThanSign();
	bool isEqualsSign();
	bool isGreaterThanSign();
	bool isQuestionMark();
	bool isGraveAccent();
	bool isEOF();
    
    bool isSmallLetter();
    bool isCapitalLetter();
    bool isWhiteSign();
    
    Character& operator= (char c);
    Character& operator= (char* c); 

    operator char();   
    
};

class AdditionalCharacter : public Character {
  
    char value;
    
public:
    
    AdditionalCharacter& operator= (char c);
    AdditionalCharacter& operator= (char* c); 
    
    operator char();  
    
};

#endif
