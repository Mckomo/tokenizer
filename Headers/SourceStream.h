//
//  SourceStream.h
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Mac__Tokenizer_SourceStream_h
#define Mac__Tokenizer_SourceStream_h

#include <string>

using namespace std;

// SourceStream

class SourceStream {
    
public:
    
    virtual char getNextChar() = 0;
    
};

// KeyboardSourceStream

class KeyboardSourceStream : public SourceStream {

public:
    
    virtual char getNextChar();
    
};

// TextSourceStream

class TextSourceStream : public SourceStream {
    
    char currentChar;

public:
    
    char *content;
    
    TextSourceStream();
    TextSourceStream(char *stream);
    virtual char getNextChar();
};

// CurlSourceStream

class CurlSourceStream : public SourceStream {
    
public:
    
    CurlSourceStream(string address);
    virtual char getNextChar();   
    
};

#endif
