//
//  Token.h
//  Mac: Tokenizer
//
//  Created by Mckomo on 17.05.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Mac__Tokenizer_Token_h
#define Mac__Tokenizer_Token_h

#include <string>
#include "Attribute.h"

using namespace std;

extern const string TokenTypeName[];

enum TokenType {
    DOCTYPE_TOKEN,
    START_TAG_TOKEN,
    END_TAG_TOKEN,
    CHARAKTER_TOKEN,
    END_OF_LINE_TOKEN,
    COMMENT_TOKEN,  
};

// Token

class Token {
    
public:
    
    TokenType type;
    void setType(TokenType tokenType);
    TokenType getType();
    string getTypeName();
    
};

// DoctypeToken

class DoctypeToken : public Token {

public:
        
    DoctypeToken();
    
};

// TagToken

class TagToken : public Token {
    
public:
    
    string name;
    Attribute attribute;
    

        
    void appendName(char c);
    
    void startNewAttribute(char c);
    
    void appendAttributeName(char c);
    void appendAttributeValue(char c);

};

// StartTagToken

class StartTagToken : public TagToken {
    
public:
    
    //Flags
    bool selfClosing;
    
    StartTagToken();
    
};

// EndTagToken

class EndTagToken : public TagToken {
    
public:
    
    EndTagToken();
    
};


// CharakterToken

class CharakterToken : public Token {
    
public:
    
    char value;
    
    CharakterToken();
    CharakterToken(char c);
    void setValue(char c);
    
};

// EndOfLineToken

class EndOfLineToken : public CharakterToken {
    
public:
    
    EndOfLineToken();
    
};

// CommentToken

class CommentToken : public CharakterToken {
    
public:
    CommentToken();
    CommentToken(char c);
    
};

#endif
